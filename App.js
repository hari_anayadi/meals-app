import { StyleSheet } from 'react-native'
import React from 'react'
import MealsNavigator from './navigation/MealsNavigator'
const App = () => {
  return (
  <MealsNavigator/>
  )
}

export default App

const styles = StyleSheet.create({})