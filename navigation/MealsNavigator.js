import { createStackNavigator} from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import CategoriesScreen from "../screens/CategoriesScreen";
import CategoryMealsScreen from "../screens/CategoryMealsScreen";
import MealDetailScreen from "../screens/MealDetailScreen";
import React from "react";




const Stack=createStackNavigator();

const MyStack=(props)=>{
    return(
        <Stack.Navigator>
            <Stack.Screen name="categoryScreen" component={CategoriesScreen} options={{headerShown:false}} />
            <Stack.Screen name="categoryMeals" component={CategoryMealsScreen}/>
            <Stack.Screen name="mealDetails" component={MealDetailScreen }/>

        </Stack.Navigator>
    )
}

const MealsNavigator=()=>{
    return(
        <NavigationContainer>
            <MyStack/>
        </NavigationContainer>
    )
}

export default MealsNavigator