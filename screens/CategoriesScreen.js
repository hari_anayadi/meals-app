import {StyleSheet, FlatList} from 'react-native';
import React from 'react';
import {CATEGORIES} from '../data/DummyData';
import CategoryMealsScreen from './CategoryMealsScreen';
import CategoryGridTile from '../components/CategoryGridTile';

const CategoriesScreen = ({navigation}, props) => {
  const renderGridItem = itemData => {
    return (
      <CategoryGridTile
        onSelect={() =>
          navigation.navigate('categoryMeals', {
            CategoryId: itemData.item.id,
          })
        }
        title={itemData.item.title}
        color={itemData.item.color}
      />
    );
  };
  return (
    <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2} />
  );
};

export default CategoriesScreen;

const styles = StyleSheet.create({});
