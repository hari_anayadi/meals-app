import {StyleSheet, Text, View, FlatList} from 'react-native';
import React, {useEffect} from 'react';
import {CATEGORIES, MEALS} from '../data/DummyData';
import MealItemComponent from '../components/MealItemComponent';

const CategoryMealsScreen = ({route, navigation}, props) => {
  const CatId = route.params;
  // console.log(CatId.CategoryId)
  const SelectedCategory = CATEGORIES.find(cat => cat.id === CatId.CategoryId);
  const MainCatId = SelectedCategory.id;
  const MainColor = SelectedCategory.color;
  // console.log(MainColor)
  //console.log(mainCatId)
  useEffect(() => {
    navigation.setOptions({
      title: SelectedCategory.title,
      headerStyle: {
        backgroundColor: MainColor,
      },
    });
  }, [SelectedCategory.title, navigation]);
  //useEffect for navigation title

  const displayedMeals = MEALS.filter(
    meal => meal.categoryIds.indexOf(MainCatId) >= 0,
  );
  //filter meals data using indexOf and stored it in dislayedmeals

  const RenderDisplayedMeals = itemData => {
    return (
      <MealItemComponent
        title={itemData.item.title}
        duration={itemData.item.duration}
        complexity={itemData.item.complexity}
        affordability={itemData.item.affordability}
        image={itemData.item.imageUrl}
        BgColor={MainColor}
      />
    );
  };

  // console.log(displayedMeals);
  return (
    <View style={{...styles.MainView, ...{backgroundColor: MainColor}}}>
      <FlatList
        data={displayedMeals}
        keyExtractor={(item, index) => item.id}
        renderItem={RenderDisplayedMeals}
      />
    </View>
  );
};

export default CategoryMealsScreen;

const styles = StyleSheet.create({
  MainView: {
    flex: 1,
  },
});
