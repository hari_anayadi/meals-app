import { StyleSheet, Text, View,TouchableNativeFeedback,TouchableOpacity } from 'react-native'
import React from 'react'

const CategoryGridTile = (props) => {
  return (
    <TouchableOpacity onPress={props.onSelect} style={{...styles.Tile,...{backgroundColor:props.color}}}>
        <View>
          <Text style={{color:"black",fontFamily:'OpenSans-Bold',fontSize:20}}>{props.title}</Text>
        </View>
      </TouchableOpacity>
  )
}

export default CategoryGridTile

const styles = StyleSheet.create({
Tile:{
    flex:1,
    margin:15,
    height:150,
    alignItems:"flex-end",
    justifyContent:"flex-end",
    elevation:5,
    padding:20
    
}
})