import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import React from 'react';

const MealItemComponent = props => {
  return (
    <View style={{...styles.MealItem,...{backgroundColor:props.BgColor,style:props.style}}}>
      <TouchableOpacity onPress={props.onSelect}>
        <View>
          <View style={{...styles.MealHeader}}>
            <ImageBackground source={{uri: props.image}} style={styles.BgImage}>
              <Text style={styles.ItemTitleStyle} numberOfLines={1}>{props.title}</Text>
            </ImageBackground>
          </View>
          <View style={{...styles.MealRow, ...styles.MealDetails}}>
            <Text style={styles.TextStyle}>{props.duration}m</Text>
            <Text style={styles.TextStyle}>
              {props.complexity.toUpperCase()}
            </Text>
            <Text style={styles.TextStyle}>
              {props.affordability.toUpperCase()}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default MealItemComponent;

const styles = StyleSheet.create({
  MealItem: {
    height: 300,
    width: '100%',
    paddingVertical:10,
    paddingHorizontal:10,
  },
  BgImage: {
    height: '100%',
    width: '100%',
    justifyContent:"flex-end"
  },
  MealRow: {
    flexDirection: 'row',
  },
  MealHeader: {
    height: '80%',
    width: '100%',
  },
  MealDetails: {
    justifyContent: 'space-between',
    width: '100%',
    height: 50,
    padding: 14,
    alignItems: 'center',
    backgroundColor:"black"
  },
  TextStyle: {
    fontFamily: 'OpenSans-Bold',
    color:"white",
  },
  ItemTitleStyle:{
      fontFamily:'OpenSans-Bold',
      fontSize:15,
      color:"white",
      backgroundColor:"black",
      paddingVertical:5,
      paddingHorizontal:12,
  }
});
